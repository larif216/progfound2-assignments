import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Park;
import javari.park.Visitor;
import javari.reader.AttractionReader;
import javari.reader.CategoriesReader;
import javari.reader.CsvReader;
import javari.reader.RecordReader;
import javari.writer.RegistrationWriter;

public class A3Festival {
    private static Scanner input = new Scanner(System.in);
    private static Visitor visitor = new Visitor();
    private static String defaultPath = "C:\\Users\\Lutfi\\Desktop\\Universitas Indonesia\\Akademis\\Semester 2\\Dasar - Dasar Pemrograman 2\\Tugas Pemrograman\\progfound2-assignments\\assignment-3\\data";

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        CsvReader[] files = new CsvReader[3];

        while (true) {
            try {
                files[0] = new CategoriesReader(Paths.get(defaultPath, "animals_categories.csv"));
                files[1] = new AttractionReader(Paths.get(defaultPath, "animals_attractions.csv"));
                files[2] = new RecordReader(Paths.get(defaultPath, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            } catch (IOException e) {
                System.out.println("\n... File not found or incorrect file!\n");
                System.out.println("Please provide the source data path: ");
                defaultPath = input.nextLine();
            }
        }

        long validCount = files[0].countValidRecords();
        long invalidCount = files[0].countInvalidRecords();
        System.out.printf("Found %d valid sections and %d invalid section %n", validCount, invalidCount);
        System.out.printf("Found %d valid attractions and %d invalid attractions%n", files[1].countValidRecords(), files[1].countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories%n", validCount, invalidCount);
        System.out.printf("Found %d valid animal records and %d invalid animal records%n", files[2].countValidRecords(), files[2].countInvalidRecords());

        System.out.println();
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("Please answer the questions by typing the number. ");
        System.out.println("Type # if you want to return to the previous menu\n");
        chooseSection();
    }

    private static void chooseSection() {
        System.out.println("Javari Park has 3 sections:");
        System.out.println("1. Explore the Mammals");
        System.out.println("2. World of Aves");
        System.out.println("3. Reptillian Kingdom");
        System.out.print("Please choose your preferred section (type the number): ");
        String choice = input.nextLine();
        if (choice.equals("#")) {
            chooseSection();
        } else if (Integer.parseInt(choice) <= Park.getSection().size()) {
            chooseAnimal(Park.getSection().get(Integer.parseInt(choice) - 1));
        } else {
            System.out.println("Input error");
            chooseSection();
        }
    }

    private static void chooseAnimal(String section) {
        System.out.println("--" + section + "--");
        ArrayList<String> animals = new ArrayList<>();
        if (section.equalsIgnoreCase("Explore the Mammals")) {
            animals = Park.getMammal();
        } else if (section.equalsIgnoreCase("World of Aves")) {
            animals = Park.getAves();
        } else if (section.equalsIgnoreCase("Reptillian Kingdom")) {
            animals = Park.getReptile();
        }

        int number = 1;
        for (String animal: animals) {
            System.out.println(number + ". " + animal);
            number++;
        }

        System.out.print("Please choose your preferred animal (type the number): ");
        String choice = input.nextLine();
        if (choice.equals("#")) {
            chooseSection();
        } else if (Integer.parseInt(choice) <= animals.size()) {
            boolean flag = false;
            String animalChoice = animals.get(Integer.parseInt(choice) - 1);
            for (Animal hewan : Park.getListOfAnimal()) {
                if (hewan.getType().equalsIgnoreCase(animalChoice) && hewan.isShowable()) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                chooseAttraction(section, animalChoice);
            } else {
                System.out.println("Unfortunately, no " + animalChoice + " can perform any attraction, please choose other animals");
                chooseAnimal(section);
            }

        } else {
            System.out.println("Input Error");
            chooseAnimal(section);
        }
    }

    private static void chooseAttraction(String section, String animal) {
        System.out.println("Attraction(s) by " + animal);
        ArrayList<String> listAtraction = new ArrayList<>();
        animal = animal.toLowerCase();
        if (Park.getCircleOfFire().contains(animal)) {
            listAtraction.add("Circles Of Fires");
        }
        if (Park.getDancingAnimals().contains(animal)) {
            listAtraction.add("Dancing Animals");
        }
        if (Park.getCountingMasters().contains(animal)) {
            listAtraction.add("Counting Masters");
        }
        if (Park.getPassionateCoders().contains(animal)) {
            listAtraction.add("Passionate Coders");
        }

        int number = 1;
        for (String att: listAtraction) {
            System.out.println(number + ". " + att);
            number++;
        }

        System.out.println("Please choose your preferred attractions (type the number): ");
        String choice = input.nextLine();
        if (choice.equals("#")) {
            chooseAnimal(section);
        } else if (Integer.parseInt(choice) <= listAtraction.size()) {
            Attraction attraction = new Attraction(listAtraction.get(Integer.parseInt(choice) - 1),
                                                    animal);
            for (Animal hewan: Park.getListOfAnimal()) {
                if (hewan.getType().equalsIgnoreCase(animal)) {
                    attraction.addPerformer(hewan);
                }
            }
            inputNama(attraction);
        } else {
            System.out.println("Wrong input");
        }
    }

    private static void inputNama(Attraction attraction) {
        System.out.println("Wow, one more step!");
        System.out.print("Please let us know your name: ");
        String name = input.nextLine();
        visitor.setVisitorName(name);
        check(visitor, attraction);
    }

    private static void check(Visitor visitor, Attraction attraction) {
        System.out.println("Yeay, last check!");
        System.out.println("Here is your data, and the attraction you chose:");
        System.out.println("Name: " + visitor.getVisitorName());
        System.out.println("Attraction: " + attraction.getName() + " -> " + attraction.getType());
        StringBuilder performer = new StringBuilder();
        for (Animal a: attraction.getPerformers()) {
            performer.append(a.getName()).append(", ");
        }
        System.out.println("With: " + performer.substring(0, performer.length() - 2));

        System.out.print("Is the data correct? (Y/N): ");
        if (input.nextLine().equalsIgnoreCase("Y")) {
            visitor.addSelectedAttraction(attraction);
            chooseAnother();
        } else {
            chooseSection();
        }
    }

    private static void chooseAnother() {
        System.out.println("Thank you for your interest. ");
        System.out.println("Would you like to register to other attractions? (Y/N): ");
        if (input.nextLine().equalsIgnoreCase("Y")) {
            chooseSection();
        } else {
            printJson();
        }
    }

    private static void printJson() {
        try {
            RegistrationWriter.writeJson(visitor, Paths.get(defaultPath));
        } catch (IOException e) {
            System.out.println("\n... Destination folder not found\n");
            System.out.println("Please provide destination folder path: ");
            defaultPath = input.nextLine();
            printJson();
        }
    }
}
