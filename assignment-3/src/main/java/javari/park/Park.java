package javari.park;

import java.util.ArrayList;

import javari.animal.Animal;
import javari.animal.aves.Aves;
import javari.animal.mammal.Mammal;
import javari.animal.reptile.Reptile;

public class Park {
    private static ArrayList<String> section = new ArrayList<>();
    private static ArrayList<String> mammal = new ArrayList<>();
    private static ArrayList<String> aves = new ArrayList<>();
    private static ArrayList<String> reptile = new ArrayList<>();
    private static ArrayList<Animal> listOfAnimal = new ArrayList<>();
    private static ArrayList<String> circleOfFire = new ArrayList<>();
    private static ArrayList<String> dancingAnimals = new ArrayList<>();
    private static ArrayList<String> countingMasters = new ArrayList<>();
    private static ArrayList<String> passionateCoders = new ArrayList<>();

    public static void addSection(String section) {
        if (!Park.section.contains(section)) {
            Park.section.add(section);
        }
    }

    public static ArrayList<String> getSection() {
        return section;
    }

    public static void addType(String sec, String type) {
        if (sec.equalsIgnoreCase("Explore the mammals") && !mammal.contains(type)) {
            mammal.add(type.toLowerCase());
        } else if (sec.equalsIgnoreCase("World of Aves") && !aves.contains(type)) {
            aves.add(type.toLowerCase());
        } else if (sec.equalsIgnoreCase("Reptillian kingdom") && !reptile.contains(type)) {
            reptile.add(type.toLowerCase());
        }
    }

    public static ArrayList<String> getMammal() {
        return mammal;
    }

    public static ArrayList<String> getAves() {
        return aves;
    }

    public static ArrayList<String> getReptile() {
        return reptile;
    }

    public static void addAnimal(String[] info) {
        if (info[1].equalsIgnoreCase("lion")) {
            listOfAnimal.add(new Mammal(info));
        } else if (info[1].equalsIgnoreCase("cat")) {
            listOfAnimal.add(new Mammal(info));
        } else if (info[1].equalsIgnoreCase("hamster")) {
            listOfAnimal.add(new Mammal(info));
        } else if (info[1].equalsIgnoreCase("whale")) {
            listOfAnimal.add(new Mammal(info));
        } else if (info[1].equalsIgnoreCase("parrot")) {
            listOfAnimal.add(new Aves(info));
        } else if (info[1].equalsIgnoreCase("eagle")) {
            listOfAnimal.add(new Aves(info));
        } else if (info[1].equalsIgnoreCase("snake")) {
            listOfAnimal.add(new Reptile(info));
        }
    }

    public static ArrayList<Animal> getListOfAnimal() {
        return listOfAnimal;
    }


    public static void addPerformer(String att, String p) {
        p = p.toLowerCase();
        if (att.equalsIgnoreCase("Circles of Fires")) {
            circleOfFire.add(p);
        }
        if (att.equalsIgnoreCase("Dancing Animals")) {
            dancingAnimals.add(p);
        }
        if (att.equalsIgnoreCase("Counting Masters")) {
            countingMasters.add(p);
        }
        if (att.equalsIgnoreCase("Passionate Coders")) {
            passionateCoders.add(p);
        }
    }

    public static ArrayList<String> getCircleOfFire() {
        return circleOfFire;
    }

    public static ArrayList<String> getDancingAnimals() {
        return dancingAnimals;
    }

    public static ArrayList<String> getCountingMasters() {
        return countingMasters;
    }

    public static ArrayList<String> getPassionateCoders() {
        return passionateCoders;
    }
}
