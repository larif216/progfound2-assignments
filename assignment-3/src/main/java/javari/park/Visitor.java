package javari.park;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the visitor of Javari Park.
 * This class implements interface Registration.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author TODO If you make changes in this class, please write your name here
 *     and describe the changes in the comment block
 */
public class Visitor implements Registration {
    private String name;
    private static int idCount = 0;
    private int id;
    private List<SelectedAttraction> selectedAttraction = new ArrayList<>();

    /**
     * Construts instance of Visitor.
     */
    public Visitor() {
        idCount += 1;
        this.id = idCount;
    }

    @Override
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    @Override
    public String getVisitorName() {
        return this.name;
    }

    @Override
    public int getRegistrationId() {
        return this.id;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttraction;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttraction.add(selected);
        return true;
    }
}
