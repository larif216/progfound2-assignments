package javari.park.attraction;

import javari.animal.Animal;
import javari.park.SelectedAttraction;
import java.util.ArrayList;
import java.util.List;

public class DancingAnimal implements SelectedAttraction{
    private String name = "Dancing Animal";
    private List<Animal> listPerformer = new ArrayList<>();
    private String type;

    public DancingAnimal(String type){
        this.type = type;
    }

    public String getName(){
        return this.name;
    }

    public String getType(){
        return this.type;
    }

    public List<Animal> getPerformers(){
        return this.listPerformer;
    }

    public boolean addPerformer(Animal performer){
        if (performer.isShowable()){
            listPerformer.add(performer);
            return true;
        }
        return false;
    }
}