package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import javari.park.Park;

public class AttractionReader extends CsvReader {
    private static String[] validAttraction = {"Circles Of Fires", "Dancing Animals",
                                               "Counting Masters", "Passionate Coders"};
    private static String[][] validPerformers = {   {"Whale", "Lion", "Eagle"},
                                                    {"Cat", "Snake", "Parrot", "Hamster"},
                                                    {"Hamster", "Whale", "Parrot"},
                                                    {"Cat", "Hamster", "Snake"}};

    public AttractionReader(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        ArrayList<String> valid = new ArrayList<>();
        for (String line: lines) {
            String[] info = line.split(",");
            for (int i = 0; i < 4; i++) {
                if (info[1].equalsIgnoreCase(validAttraction[i])) {
                    for (String passed: validPerformers[i]) {
                        if (info[0].equalsIgnoreCase(passed)) {
                            Park.addPerformer(validAttraction[i], passed);
                            if (!valid.contains(validAttraction[i])) {
                                valid.add(validAttraction[i]);
                            }
                        }
                    }
                }
            }
        }
        return valid.size();
    }

    @Override
    public long countInvalidRecords() {
        int invalid = 0;
        for (String line: lines) {
            String[] info = line.split(",");
            boolean valid = false;
            for (int i = 0; i < 4; i++) {
                if (info[1].equalsIgnoreCase(validAttraction[i])) {
                    for (String p: validPerformers[i]) {
                        if (info[0].equals(p)) {
                            valid = true;
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                invalid++;
            }
        }
        return invalid;
    }
}
