package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import javari.park.Park;

public class RecordReader extends CsvReader {
    private int invalid = 0;
    private int valid = 0;

    public RecordReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        for (String line: lines) {
            String[] info = line.split(",");
            try {
                if (Park.getMammal().contains(info[1].toLowerCase())
                        || Park.getAves().contains(info[1].toLowerCase())
                        || Park.getReptile().contains(info[1].toLowerCase())) {
                    Park.addAnimal(info);
                    valid += 1;
                } else {
                    invalid++;
                }
            } catch (Exception e) {
                invalid++;
            }
        }
        return valid;
    }

    public long countInvalidRecords() {
        return invalid;
    }
}

