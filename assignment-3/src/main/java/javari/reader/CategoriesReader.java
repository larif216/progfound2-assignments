package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javari.park.Park;

/**
 * This class represents CategoriesReader to read animal's section and
   category from input file.
 * This class extends abstract class CsvReader
 * @author Irwanto
 */

public class CategoriesReader extends CsvReader {
    private static String[] validCategory = {"Mammals", "Aves", "Reptiles"};
    private static String[] validSection = {"Explore the Mammals", "World of Aves",
                                            "Reptillian Kingdom"};
    private static String[][] validType = { {"Whale", "Lion", "Cat", "Hamster"},
                                            {"Parrot", "Eagle"},
                                            {"Snake"}};

    public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        for (String line: lines) {
            String[] info = line.split(COMMA);
            for (int i = 0; i < 3; i++) {
                if (info[2].equalsIgnoreCase(validSection[i])
                    && info[1].equalsIgnoreCase(validCategory[i])) {
                    for (int j = 0; j < validType[i].length; j++) {
                        if (info[0].equalsIgnoreCase(validType[i][j])) {
                            Park.addSection(validSection[i]);
                            Park.addType(validSection[i], validType[i][j]);
                        }
                    }
                }
            }
        }
        return Park.getSection().size();
    }

    /**
     * Returns the numbers of invalid input on the input file
     * @return
     */
    public long countInvalidRecords() {
        int invalid = 0;
        for (String line: lines) {
            String[] info = line.split(COMMA);
            boolean valid = false;
            for (int i = 0; i < 3; i++) {
                if (info[2].equalsIgnoreCase(validSection[i])
                    && info[1].equalsIgnoreCase(validCategory[i])) {
                    for (int j = 0; j < validType[i].length; j++) {
                        if (info[0].equalsIgnoreCase(validType[i][j])) {
                            valid = true;
                            break;
                        }
                    }
                }
            }
            if (!valid) {
                invalid++;
            }
        }
        return invalid;
    }
}
