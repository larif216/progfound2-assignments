package javari.animal.mammal;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public class Mammal extends Animal {
    private String specialStatus;

    public Mammal(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]),
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specialStatus = info[6];
    }

    @Override
    public boolean specificCondition() {
        return !(specialStatus.equalsIgnoreCase("pregnant")
                || (this.getType().equalsIgnoreCase("Lion") && this.getGender() == Gender.FEMALE));
    }
}
