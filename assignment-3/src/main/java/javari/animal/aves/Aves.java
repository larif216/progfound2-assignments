package javari.animal.aves;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public class Aves extends Animal {
    private String specialStatus;

    public Aves(String[] info) {
        super(Integer.parseInt(info[0]), info[1], info[2], Gender.parseGender(info[3]),
                Double.parseDouble(info[4]), Double.parseDouble(info[5]),
                Condition.parseCondition(info[7]));
        this.specialStatus = info[6];
    }

    @Override
    public boolean specificCondition() {
        return !(this.specialStatus.equalsIgnoreCase("laying eggs"));
    }
}
