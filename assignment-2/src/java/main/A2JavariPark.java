import java.util.*;
import java.lang.*;
import Animal.*;
import Cage.*;

public class A2JavariPark{
    public void process(String animal){
		boolean retry = true;
		while (retry){
			System.out.format("%s: ", animal);
			Scanner input = new Scanner(System.in);
			try{
				int num = Integer.parseInt(input.nextLine());
				if (num < 0){
					System.out.println("Input can't be negative number!");
					continue;
				} 
				// store num of animal
				if (animal.equals("cat")) Cat.numOfCat = num;
				else if (animal.equals("lion")) Lion.numOfLion = num;
				else if (animal.equals("eagle")) Eagle.numOfEagle = num;
				else if (animal.equals("parrot")) Parrot.numOfParrot = num;
				else if (animal.equals("hamster")) Hamster.numOfHamster = num;
				if (num == 0){ // skip kalo jumlah hewannya 0
					return;
				}else{
					System.out.format("Provide the information of %s(s): ", animal);
					String animalData = input.nextLine();
					String[] animalDataArr = animalData.split(",");
					if (animalDataArr.length != num){ // nyocokin jumlah hewan sama jumlah data yang diinput
						System.out.println("Information not match with num of animal");
						continue; 
					} else{
						retry = false; // kalo bener, ga ngulang lagi
					}
					Cage.makeCages(animalDataArr, animal);
				}	
			}catch(Exception e){
				System.out.println("Invalid! Input must be integer");
				continue;
			}
		}
	}

    public static void main(String[] args){
        String[] animals = {"cat", "lion", "eagle", "parrot", "hamster"};
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari park!");
        System.out.println("Input the number of animals");

		A2JavariPark javariPark = new A2JavariPark();
        for (String animal : animals) javariPark.process(animal); //iterasi elemen animals, proses setiap elemen
		// selesai memasukkan semua data hewan
        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");
		// mulai arrangement
        System.out.println("Cage arrangement:");
        Arrangement.arrangement();
		// selesai arrangement
        System.out.println("NUMBER OF ANIMALS:");
		System.out.println("cat: " + Cat.numOfCat);
		System.out.println("lion: " + Lion.numOfLion);
		System.out.println("parrot: " + Parrot.numOfParrot);
		System.out.println("eagle: "+ Eagle.numOfEagle);
		System.out.println("hamster: " + Hamster.numOfHamster);
        System.out.println("\n=============================================");
        
        while(true){
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
			try{
				int pick = Integer.parseInt(input.nextLine());
				String animalName = "";
			
			if (pick == 1){
				if (Cat.numOfCat > 0){
					System.out.print("Mention the name of cat you want to visit: ");
					animalName = input.nextLine();
					
					boolean exist = false;
					for(int i = 0; i < Cat.catList.size(); i++) if (animalName.equals(Cat.catList.get(i).getName())) {
						exist = true;
						Cat.catList.get(i).selected();
						break;
					}
					if (!exist){
						System.out.println("There is no cat with that name! Back to the office!\n");
						continue;
					}
				}else{
					System.out.println("There is no Cat\n");
					continue;
				}
			}
			else if (pick == 2){
				if (Eagle.numOfEagle > 0){
					System.out.print("Mention the name of eagle you want to visit: ");
					animalName = input.nextLine();
					
					boolean exist = false;
					for(int i = 0; i < Eagle.eagleList.size(); i++) if (animalName.equals(Eagle.eagleList.get(i).getName())){
						exist = true;
						Eagle.eagleList.get(i).selected();
						break;
					}
					if (!exist){
						System.out.println("There is no eagle with that name! Back to the office!\n");
						continue;
					}
				}else{
					System.out.println("There is no Eagle\n");
					continue;
				}
			}
			else if (pick == 3){
				if (Hamster.numOfHamster > 0){
					System.out.print("Mention the name of hamster you want to visit: ");
					animalName = input.nextLine();
					
					boolean exist = false;
					for(int i = 0; i < Hamster.hamsterList.size(); i++) if (animalName.equals(Hamster.hamsterList.get(i).getName())){
						exist = true;
						Hamster.hamsterList.get(i).selected();
						break;
					}
					if (!exist){
						System.out.println("There is no hamster with that name! Back to the office!\n");
						continue;
					}
				}else{
					System.out.println("There is no Hamster\n");
					continue;
				}
			}
			else if (pick == 4){
				if (Parrot.numOfParrot > 0){
					System.out.print("Mention the name of parrot you want to visit: ");
					animalName = input.nextLine();
					
					boolean exist = false;
					for(int i = 0; i < Parrot.parrotList.size(); i++) if (animalName.equals(Parrot.parrotList.get(i).getName())){
						exist = true;
						Parrot.parrotList.get(i).selected();
						break;
					}
					if (!exist){
						System.out.println("There is no parrot with that name! Back to the office!\n");
						continue;
					}
				}else{
					System.out.println("There is no Parrot\n");
					continue;
				}
			}
			else if (pick == 5){
				if (Lion.numOfLion > 0){
					System.out.print("Mention the name of lion you want to visit: ");
					animalName = input.nextLine();
					
					boolean exist = false;
					for(int i = 0; i < Lion.lionList.size(); i++) if (animalName.equals(Lion.lionList.get(i).getName())){
						exist = true;
						Lion.lionList.get(i).selected();
						break;
					}
					if (!exist){
						System.out.println("There is no lion with that name! Back to the office!\n");
						continue;
					}
				}else{
					System.out.println("There is no Lion\n");
					continue;
				}
			}
			else if (pick == 99){
				break;
			}
			else System.out.println("Invalid input!"); // input selain valid, ngulang lagi
			}catch(Exception e){
				System.out.println("Invalid! Input must be integer! yhaa");
				continue;
		}
	}
	}
}