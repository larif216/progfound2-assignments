package Animal;

import java.util.*;
import java.lang.*;

public class Parrot extends Animal{
    public static int numOfParrot;
    public static ArrayList<Parrot> parrotList = new ArrayList<>();

    public Parrot(String name, int bodyLength){
        super(name, bodyLength, false);
    }

    public void selected(){
        System.out.format("You are visiting %s (parrot) now, what would you like to do?\n", this.getName());
        System.out.println("1: Order to fly 2: Do conversation");
        Scanner input = new Scanner(System.in);
        int command = Integer.parseInt(input.nextLine());
        if (command == 1){
            this.fly();
        }else if (command == 2){
            System.out.print("You say: ");
            String word = input.nextLine();
            this.conversation(word);
        }else{
            System.out.format("%s says: HM?\n", this.getName());
        }
        System.out.println("Back to the office!\n");
    }
    
    public void fly(){
        System.out.format("Parrots %s flies!\n", this.getName());
        System.out.format("%s makes a voice: TERBAAANG...\n", this.getName(), this.getName());
    }
    public void conversation(String word){
        System.out.format("%s says: %s\n", this.getName(), word);
    }
}