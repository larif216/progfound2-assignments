package Animal;

import java.util.*;
import java.lang.*;

public class Lion extends Animal{
    public static int numOfLion;
    public static ArrayList<Lion> lionList = new ArrayList<>();

    public Lion(String name, int bodyLength){
        super(name, bodyLength, true);
    }

    public void selected(){
        System.out.format("You are visiting %s (lion) now, what would you like to do?\n", this.getName());
        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
        Scanner input = new Scanner(System.in);
        int command = Integer.parseInt(input.nextLine());
        if (command == 1){
            this.hunting();
        }else if (command == 2){
            this.brush();
        }else if (command == 3){
            this.disturb();
        }else{
            System.out.println("You do nothing...");
        }
        System.out.println("Back to the office!\n");
    }

    public void hunting(){
        System.out.println("Lion is hunting..");
        System.out.format("%s makes a voice: err....!\n", this.getName());
    }
    public void brush(){
        System.out.println("Clean the the lion's mane..");
        System.out.format("%s makes a voice: Hauhhmm !\n", this.getName());
    }
    public void disturb(){
        System.out.format("%s makes a voice: HAUHHMM !\n", this.getName()); 
    }
}