package Animal;

import java.util.*;
import java.lang.*;

public class Cat extends Animal{
    public static int numOfCat;
    public static ArrayList<Cat> catList = new ArrayList<>();
    private static String[] sounds = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

    public Cat(String name, int bodyLength){
        super(name, bodyLength, false);
    }

    public void selected(){
        System.out.format("You are visiting %s (cat) now, what would you like to do?\n", this.getName());
        System.out.println("1: Brush the fur 2: Cuddle");
        Scanner input = new Scanner(System.in);
        int command = Integer.parseInt(input.nextLine());
        if (command == 1){
            this.getBrush();
        }else if (command == 2){
            this.getCuddled();
        }else{
            System.out.println("You do nothing...");
        }
        System.out.println("Back to the office!\n");
    }

    public void getBrush(){
        System.out.format("Time to clean %s's fur\n", this.getName());
        System.out.format("%s makes a voice: Nyaaan...\n", this.getName(), this.getName());
    }

    public void getCuddled(){
        Random random = new Random();
        int randomIndex = random.nextInt(Cat.sounds.length);
        System.out.format("%s makes a voice: %s\n", this.getName(), Cat.sounds[randomIndex]);
    }
}