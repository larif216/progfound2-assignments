package Animal;

import java.util.*;
import java.lang.*;

public class Animal{

    protected String name;
    protected int bodyLength;
    protected String cage;
    protected int cageLength, cageWidth;
    protected boolean cageType; //false = indoor, true = outdoor

    public Animal(String name, int bodyLength, boolean cageType){
        this.name = name;
        this.bodyLength = bodyLength;
        this.cageType = cageType;
        if (cageType){
            if(bodyLength <= 75){
                this.cageLength = 120;
                this.cageWidth = 120;
                this.cage = "A";
            }else if(bodyLength <= 90){
                this.cageLength = 120;
                this.cageWidth = 150;
                this.cage = "B";
            }else{
                this.cageLength = 120;
                this.cageWidth = 180;
                this.cage = "C";
            }
        }else{
            if (bodyLength <= 45){
                this.cageLength = 60;
                this.cageWidth = 60;
                this.cage = "A";
            }else if (bodyLength <= 60){
                this.cageLength = 60;
                this.cageWidth = 90;
                this.cage = "B";
            }else{
                this.cageLength = 60;
                this.cageWidth = 120;
                this.cage = "C";
            }
        }
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setBodyLength(int bodyLength){
        this.bodyLength = bodyLength;
    }
    public int getBodyLength(){
        return this.bodyLength;
    }
    public void setCage(String cage){
        this.cage = cage;
    }
    public String getCage(){
        return this.cage;
    }
}