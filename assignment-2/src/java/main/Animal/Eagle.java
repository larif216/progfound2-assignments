package Animal;

import java.util.*;
import java.lang.*;

public class Eagle extends Animal{
    public static int numOfEagle;
    public static ArrayList<Eagle> eagleList = new ArrayList<>();

    public Eagle(String name, int bodyLength){
        super(name, bodyLength, true);
    }

    public void selected(){
        System.out.format("You are visiting %s (eagle) now, what would you like to do?\n", this.getName());
        System.out.println("1: Order to fly");
        Scanner input = new Scanner(System.in);
        int command = Integer.parseInt(input.nextLine());
        if (command == 1){
            this.fly();
        }else{
            System.out.println("You do nothing...");
        }
        System.out.println("Back to the office!\n");
    }

    public void fly(){
        System.out.format("%s makes a voice:  kwaakk…\n", this.getName());
        System.out.println("You hurt!");
    }
}