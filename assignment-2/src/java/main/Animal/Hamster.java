package Animal;

import java.util.*;
import java.lang.*;


public class Hamster extends Animal{
    public static int numOfHamster;
    public static ArrayList<Hamster> hamsterList = new ArrayList<>();

    public Hamster(String name, int bodyLength){
        super(name, bodyLength, false);
    }

    public void selected(){
        System.out.format("You are visiting %s (hamster) now, what would you like to do?\n", this.getName());
        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
        Scanner input = new Scanner(System.in);
        int command = Integer.parseInt(input.nextLine());
        if (command == 1){
            this.gnaw();
        }else if (command == 2){
            this.run();
        }else{
            System.out.println("You do nothing...");
        }
        System.out.println("Back to the office!\n");
    }

    public void gnaw(){
        System.out.format("%s makes a voice: Ngkkrit.. Ngkkrrriiit\n", this.getName());
    }
    
    public void run(){
        System.out.format("%s makes a voice: Trrr... Trrr...\n", this.getName());
    }
}