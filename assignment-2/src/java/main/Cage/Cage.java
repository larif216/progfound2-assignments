package Cage;

import java.util.ArrayList;
import java.lang.*;
import Animal.*;

public class Cage{
    public Cat cat;
    private Lion lion;
    private Eagle eagle;
    private Parrot parrot;
    private Hamster hamster;

    // variabel dibawah untuk menampung cages berdasarkan levelnya (index 0 = level 1, dst)
    public static ArrayList<Cage[]> catCages     = new ArrayList<>();
    public static ArrayList<Cage[]> lionCages    = new ArrayList<>();
    public static ArrayList<Cage[]> eagleCages   = new ArrayList<>();
    public static ArrayList<Cage[]> parrotCages  = new ArrayList<>();
    public static ArrayList<Cage[]> hamsterCages = new ArrayList<>();

    public static final int CAGE_LEVELS = 3;

    public static void makeCages(String[] animalDataArr, String typeOfAnimal) { // element = nama|panjang
        ArrayList<Cage> tempCages = new ArrayList<>(); // isinya kumpulan single cage
        for (int index = 0; index < animalDataArr.length; index++){ // buat misahin nama sama panjang
            String[] dataArr = animalDataArr[index].split("\\|");
            String animalName = dataArr[0];
            int animalLength = Integer.parseInt(dataArr[1]);

            Cage singleCage; // insiasi hewan lalu masukin kandang
            if (typeOfAnimal.equals("cat")){
                Cat animalObj = new Cat(animalName, animalLength);
                singleCage = new Cage(animalObj);
                Cat.catList.add(animalObj);
            }else if (typeOfAnimal.equals("lion")){
                Lion animalObj = new Lion(animalName, animalLength);
                singleCage = new Cage(animalObj);
                Lion.lionList.add(animalObj);
            }else if (typeOfAnimal.equals("eagle")){
                Eagle animalObj = new Eagle(animalName, animalLength);
                singleCage = new Cage(animalObj);
                Eagle.eagleList.add(animalObj);
            }else if (typeOfAnimal.equals("parrot")){
                Parrot animalObj = new Parrot(animalName, animalLength);
                singleCage = new Cage(animalObj);
                Parrot.parrotList.add(animalObj);
            }else{
                Hamster animalObj = new Hamster(animalName, animalLength);
                singleCage = new Cage(animalObj);
                Hamster.hamsterList.add(animalObj);
            }
            tempCages.add(singleCage);
        }
        plotCages(tempCages, typeOfAnimal); // tempCages disini sudah berisi singleCage2, lalu diplot levelnya
    }

    public static void plotCages(ArrayList<Cage> cages, String typeOfAnimal){
        Cage[] tempCageLevel = new Cage[cages.size()]; // isinya singleCage2 yang udah se-level
        if (cages.size() >= Cage.CAGE_LEVELS){ // kalo jumlah cage lebih dari tiga, bagi rata, kalo ada sisa taro di level 3
            int cagePerLevel = cages.size() / Cage.CAGE_LEVELS;
            int cageRemainder = cages.size() % Cage.CAGE_LEVELS;
            for (int i = 0; i < Cage.CAGE_LEVELS; i++) { // buat inisiasi array kosong dengan slot sesuai pembagiannya
                if (i != Cage.CAGE_LEVELS - 1) { // ini buat level 1 atau 2
                    tempCageLevel = new Cage[cagePerLevel];
                } else { // ini buat level 3
                    tempCageLevel = new Cage[cagePerLevel + cageRemainder];
                }
                
                for (int j = 0; j < tempCageLevel.length; j++) { // masukin cage ke array kosong tadi
                    tempCageLevel[j] = cages.get(0);
                    cages.remove(0);
                }
                insertCage(tempCageLevel, typeOfAnimal); // nanti jadinya ArrayList of Array
            }
        } else {
            for (int num = 0; num < Cage.CAGE_LEVELS - 1; num++) {
                cages.add(null);
            }

            for (int level = 0; level < Cage.CAGE_LEVELS; level++) {
                tempCageLevel = new Cage[Cage.CAGE_LEVELS - 2]; // 1 level 1 cage
                if (level == 0) {
                    tempCageLevel[0] = cages.get(level); // isinya dari level 1
                } else { // buat level 2 sama 3 ada 2 kemungkinan
                    if (cages.get(level) != null) {
                        tempCageLevel[0] = cages.get(level);
                    } else {
                        tempCageLevel[0] = null;
                    }
                }
                insertCage(tempCageLevel, typeOfAnimal); // nanti jadinya ArrayList of Array
            }   
        }
    }

    public static void insertCage(Cage[] tempCageLevel, String typeOfAnimal){
        if (typeOfAnimal.equals("cat")){
            Cage.catCages.add(tempCageLevel);
        }else if (typeOfAnimal.equals("lion")){
            Cage.lionCages.add(tempCageLevel);
        }else if (typeOfAnimal.equals("eagle")){
            Cage.eagleCages.add(tempCageLevel);
        }else if (typeOfAnimal.equals("parrot")){
            Cage.parrotCages.add(tempCageLevel);
        }else{
            Cage.hamsterCages.add(tempCageLevel);
        }
    }

    public Cage(Cat cat){
        this.cat = cat;
    }

    public Cage(Lion lion){
        this.lion = lion;
    }

    public Cage(Eagle eagle){
        this.eagle = eagle;
    }

    public Cage(Parrot parrot){
        this.parrot = parrot;
    }
    
    public Cage(Hamster hamster){
        this.hamster = hamster;     
    }

    public String toString(String typeOfAnimal){
        if (typeOfAnimal.equals("cat")){
            return this.cat.getName() + " (" + this.cat.getBodyLength() + " - " + this.cat.getCage() + ")";
        }else if (typeOfAnimal.equals("lion")){
            return this.lion.getName() + " (" + this.lion.getBodyLength() + " - " + this.lion.getCage() + ")";
        }else if (typeOfAnimal.equals("eagle")){
            return this.eagle.getName() + " (" + this.eagle.getBodyLength() + " - " + this.eagle.getCage() + ")";
        }else if (typeOfAnimal.equals("parrot")){
            return this.parrot.getName() + " (" + this.parrot.getBodyLength() + " - " + this.parrot.getCage() + ")";
        }else{
            return this.hamster.getName() + " (" + this.hamster.getBodyLength() + " - " + this.hamster.getCage() + ")";
        }
    }
}