package Cage;

import java.util.*;
import java.lang.*;
import Animal.*;
public class Arrangement{
    public static void arrangement(){
        if (Cat.numOfCat > 0) {
            System.out.println("location: indoor");
            Arrangement.manageCagesArrangement(Cage.catCages, "cat");
        }

        if (Lion.numOfLion > 0) {
            System.out.println("location: outdoor");
            Arrangement.manageCagesArrangement(Cage.lionCages, "lion");
        }
        
        if (Eagle.numOfEagle > 0) {
            System.out.println("location: outdoor");
            Arrangement.manageCagesArrangement(Cage.eagleCages, "eagle");
        }

        if (Parrot.numOfParrot > 0) {
            System.out.println("location: indoor");
            Arrangement.manageCagesArrangement(Cage.parrotCages, "parrot");
        }

        if (Hamster.numOfHamster > 0 ) {
            System.out.println("location: indoor");
            Arrangement.manageCagesArrangement(Cage.hamsterCages, "hamster");
        }
    }
    public static void swapCages(ArrayList<Cage[]> cages) { // swap level
        Collections.swap(cages, 0, 2);
        Collections.swap(cages, 1, 2); 
    }

    public static void reverseCages(ArrayList<Cage[]> cages) {
        for (int i = 0; i < cages.size(); i++) { // iterasi level nya
            for (int j = 0; j < cages.get(i).length / 2; j++) { // iterasi element tiap level
                Cage temp = cages.get(i)[j];
                cages.get(i)[j] = cages.get(i)[cages.get(i).length - j - 1]; // tuker elemen pertama sama terakhir, mirip algoritma palindrom
                cages.get(i)[cages.get(i).length - j - 1] = temp;
            }
        }
    }

    public static void displayArrangement(ArrayList<Cage[]> cages, String animalType) {
        if (cages.size() > 0) { // hanya ngeprint yang ada hewannya (jumlah hewan != 0)
            for (int level = 0; level < Cage.CAGE_LEVELS; level++) { // iterasi tiap levelnya
                System.out.print("level " + (Cage.CAGE_LEVELS - level) + ": ");
                Cage[] printedLevel = cages.get(2 - level); // ngeprintnya dari level 3
                for (int num = 0; num < printedLevel.length; num++) { 
                    if (printedLevel[num] != null) { // kalo elemennya null, sekipp
                        System.out.print(printedLevel[num].toString(animalType) + ", ");
                    }
                }
                System.out.println();
            }
        }
    }

    public static void manageCagesArrangement(ArrayList<Cage[]> cages, String animalType) {
        displayArrangement(cages, animalType);
        System.out.println("\nAfter rearrangement...");
        swapCages(cages);
        reverseCages(cages);
        displayArrangement(cages, animalType);
        System.out.println();
    }
}