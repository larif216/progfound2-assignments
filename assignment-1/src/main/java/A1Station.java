import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {

    // Instance variable
    private static final double THRESHOLD = 250; // in kilograms
    private static ArrayList<WildCat> listCat = new ArrayList<WildCat>();
    private static ArrayList<TrainCar> listCar = new ArrayList<TrainCar>();
    private static String category;
    private static int trainQueue = 0;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try{
            int numberCat = Integer.parseInt(input.nextLine()); // Jumlah kucing yang akan di input?

            /*Block code di bawah akan meminta input dari user sebanyak numberCat
            input berupa <name,weight,length> yang mana akan dipisah dengan "," ke
            dalam Array catData dan menginisiasi objek kucing dengan atribut sesuai 
            input dan di tambahkan ke dalam ArrayList listCat.*/
            for (int i = 0; i < numberCat; i++){
                String   catInput  = input.nextLine();
                String[] catData   = catInput.split(",");
                String   catName   = catData[0];
                double   catWeight = Double.parseDouble(catData[1]);
                double   catLength = Double.parseDouble(catData[2]);
                if (catData.length > 3 || catName.equals("")){
                    System.out.print("Invalid Input");
                    System.exit(0);
                }
                WildCat  newCat    = new WildCat(catName, catWeight, catLength);
                listCat.add(newCat);

                if (i == 0){                                          // Jika index pertama (FirstCar -> Hanya menampung objek kucing)
                    TrainCar firstCar = new TrainCar(listCat.get(i)); 
                    listCar.add(firstCar);                            
                    trainQueue += 1;                                  
                    if (numberCat == 1) depart(firstCar, trainQueue); // Jika ia kucing pertama dan satu-satunya
                }
                else{                                                                          // Bukan index pertama(newCar -> Menampung kucing dan objek mobil sebelumnya)
                    TrainCar newCar = new TrainCar(listCat.get(i), listCar.get(i-1));           
                    listCar.add(newCar);
                    trainQueue += 1;
                    if (listCar.get(i).computeTotalWeight() > THRESHOLD | i == numberCat-1){    // Jika berat total melebihi THRESHOLD atau kucing terakhir yg diinput
                        depart(listCar.get(i), trainQueue);
                        if (i != numberCat-1){
                            numberCat -= (i+1);                                                 // Ubah numberCat menjadi jumlah sisa kucing yg belum diinput
                            i = -1;                                                             // Mengubah i selanjutnya menjadi 0 supaya masuk ke dalam kondisi FirstCar
                            trainQueue = 0;                                                     // Kosongkan antrean
                            listCat.clear();                                                    // Kosongkan listCat yang sudah berangkat
                            listCar.clear();                                                    // Kosongkan listCat yang sudah berangkat
                        }
                    }
                }
            }
        }
        catch(Exception e){
            System.out.print("Invalid input");
        }
        input.close();
    }

    public static void depart(TrainCar car, int trainQueue) {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        car.printCar();
        double averageMassIndex = car.computeTotalMassIndex() / trainQueue;
        System.out.format("\nAverage mass index of all cats: %.2f\n", averageMassIndex);

        if (averageMassIndex < 18.5){
            category = "underweight";
        }else if (averageMassIndex >= 18.5 && averageMassIndex < 25){
            category = "normal";
        }else if (averageMassIndex >= 25 && averageMassIndex < 30){
            category = "overweight";
        }else if (averageMassIndex >= 30){
            category = "obese";
        }
        System.out.format("In average, the cats in the train are *%s*\n", category);
    }
}