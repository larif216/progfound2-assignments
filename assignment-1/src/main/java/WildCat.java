public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name   = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        double bmi = this.weight * 10000 / Math.pow(this.length, 2); // Convert length (cm) to meters 
        return bmi;
    }
}
