public class TrainCar {

    WildCat cat;
    TrainCar next;
    public static final double EMPTY_WEIGHT = 20; // In kilograms

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        double carWeight = EMPTY_WEIGHT + this.cat.weight;
        if (this.next == null){ // Base case
            return carWeight;
        }else{
            return carWeight + this.next.computeTotalWeight(); // Recursive Case
        }
    }

    public double computeTotalMassIndex() {
        if (this.next == null){ // Base case
            return cat.computeMassIndex();
        }else{
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex(); // Recursive Case 
        }
    }

    public void printCar() {
        if (this.next == null){
            System.out.print("(" + this.cat.name + ")");
        }else{
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar();
        }
    }
}
