import javax.swing.JButton;
import javax.swing.ImageIcon;

public class Card {
    private int id;
    private JButton button = new JButton();
    private ImageIcon image;
    private static final ImageIcon COVER = GameSystem.resize(new ImageIcon("img/background.png"));

    /**
     * Create constructor of Card.
     * @param id            id of the card
     * @param image         image behind the button
     */
    public Card(int id, ImageIcon image) {
        this.id = id;
        this.image = image;
        this.button.setIcon(COVER);
    }

    /**
     * Accessor to get id of a card.
     * @return id of the card
     */
    public int getId() {
        return this.id;
    }

    /**
     * Accessor for JButton.
     * @return button
     */
    public JButton getButton() {
        return button;
    }

    /**
     * Method to show the image behind the cover.
     */
    public void clicked() {
        button.setIcon(image);
    }

    /**
     * Method to show the cover.
     */
    public void release() {
        button.setIcon(COVER);
    }
}