import javax.swing.SwingUtilities;

public class MemoryGameMain {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new GameSystem());
    }
}