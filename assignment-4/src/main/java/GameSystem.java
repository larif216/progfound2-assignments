import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.WindowConstants;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class GameSystem extends JFrame {

    private static final int PAIRS = 18;
    private static final int TWICE = 2;
    private static final int ROW_CARD = 6;
    private static final int COLUMN_CARD = 6;
    private ArrayList<Card> cards = new ArrayList<>();
    private Card firstCard;
    private Card secondCard;
    private Timer timer;
    private Timer openTime;
    private JFrame mainFrame;
    private JLabel pairsLabel;
    private JLabel stepLabel;
    private JLabel tryLabel;
    private JLabel resetLabel;
    private JPanel buttonPanel;
    private JPanel menuPanel;
    private JButton exitButton;
    private JButton resetButton;
    private int reset = 0;
    private int attemps = 0;
    private int matchedCard = 0;

    /**
     * Constructor of board class.
     */
    public GameSystem() {
        for (int i = 0; i < PAIRS; i++) {
            try {
                Image fileImage = ImageIO.read(getClass().getResource("img/" + i + ".png"));
                ImageIcon imageCard = new ImageIcon(fileImage);
                for (int j = 0; j < TWICE; j++) {
                    cards.add(new Card(i, resize(imageCard)));
                }
            } catch (Exception e) {
                System.out.println("Some file are missing error");
                showErrorWindow();
            }
        }
        Collections.shuffle(cards);
        // Set up the timer
        timer = new Timer(700, actionPerformed -> checkCards());
        timer.setRepeats(false);
        createMainFrame();
    }

    /**
     * Method to create main frame.
     */
    public void createMainFrame() {
        mainFrame = new JFrame("Match Pair Memory Games Java by Lutfi");
        mainFrame.setLayout(new BorderLayout());
        createButtonPanel();
        createMenuPanel();
        mainFrame.add(buttonPanel, BorderLayout.CENTER);
        mainFrame.add(menuPanel, BorderLayout.SOUTH);
        mainFrame.setPreferredSize(new Dimension(720, 720));
        mainFrame.setLocation(250, 250);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);

    }

    /**
     * Method to create button panel.
     */
    public void createButtonPanel() {
        buttonPanel = new JPanel(new GridLayout(ROW_CARD, COLUMN_CARD));
        for (Card card : cards) {
            card.getButton().addActionListener(actionPerformed -> selected(card));
            card.release();
            card.getButton().setEnabled(true);
            buttonPanel.add(card.getButton());
        }
    }

    /**
     * Method to create menu panel.
     */
    public void createMenuPanel() {
        menuPanel = new JPanel(new GridLayout(3, 3));
        pairsLabel = new JLabel("Number of Pairs : " + matchedCard + "/18");
        pairsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        menuPanel.add(pairsLabel);
        stepLabel = new JLabel("Current Step : Select First Card");
        stepLabel.setHorizontalAlignment(SwingConstants.CENTER);
        menuPanel.add(stepLabel);
        resetButton = new JButton("Reset Game");
        exitButton = new JButton("Exit");
        resetButton.addActionListener(actionPerformed -> resetGame(cards));
        exitButton.addActionListener(actionPerformed -> exitGame());
        menuPanel.add(resetButton);
        menuPanel.add(exitButton);
        resetLabel = new JLabel("Number of Reset : " + reset);
        tryLabel = new JLabel("Total attemps : " + attemps);
        resetLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        menuPanel.add(resetLabel);
        menuPanel.add(tryLabel);
    }

    /**
     * Method to resetGame card.
     *
     * @param cards all cards that will be resetGame
     */
    private void resetGame(ArrayList<Card> cards) {
        mainFrame.remove(buttonPanel);
        Collections.shuffle(cards);
        createButtonPanel();
        mainFrame.add(buttonPanel, BorderLayout.CENTER);
        reset++;
        attemps = 0;
        matchedCard = 0;
        resetLabel.setText("Number of Reset : " + reset);
        tryLabel.setText("Total attemps : " + attemps);
        pairsLabel.setText("Number of Pairs : " + matchedCard + "/18");
        revealAllCards();
    }

    /**
     * Method to exit the game when user clicked the button.
     */
    private void exitGame() {
        System.exit(0);
    }

    /**
     * Method to reveal all cards at the beginning of the game.
     */
    public void revealAllCards() {
        for (Card card : cards) {
            card.clicked();
        }
        buttonPanel.updateUI();
        openTime = new Timer(2500, actionPerformed -> hideAllCards());
        openTime.setRepeats(false);
        openTime.start();
    }

    /**
     * Method to hide all cards after reveal all of the card at beginning.
     */
    public void hideAllCards() {
        for (Card card : cards) {
            card.release();
        }
        buttonPanel.updateUI();
    }

    /**
     * Method to open up card selected.
     * @param selectedCard  card that is selected
     */
    public void selected(Card selectedCard) {
        if (firstCard == null) {
            firstCard = selectedCard;
            firstCard.clicked();
            stepLabel.setText("Current Step : Select Another Card");
        }

        if (firstCard != selectedCard && secondCard == null) {
            secondCard = selectedCard;
            secondCard.clicked();
            timer.start();
        }
    }

    /**
     * Method to check the cards, whether it match or not.
     * Then reset the reference of first and second card.
     */
    public void checkCards() {
        attemps++;
        tryLabel.setText("Total attemps : " + attemps);
        if (firstCard.getId() == secondCard.getId()) {
            firstCard.getButton().setEnabled(false);
            secondCard.getButton().setEnabled(false);
            matchedCard++;
            pairsLabel.setText("Number of Pairs : " + matchedCard + "/18");
            if (matchedCard == 18) {
                showWinWindow();
            }
        } else {
            firstCard.release();
            secondCard.release();
        }
        stepLabel.setText("Current Step : Select First Card");
        firstCard = null;
        secondCard = null;
    }

    /**
     * Show pop up window when game is won.
     */
    public void showWinWindow() {
        Object[] choices = { " Quit", " Play Again!" };
        int option = JOptionPane.showOptionDialog(null,
                "Congratulations! You Win!",
                "Dialog Box", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, choices, choices[1]);
        if (option == 1) {
            resetGame(cards);
        } else {
            exitGame();
        }
    }

    /**
     * Show error message.
     */
    public void showErrorWindow() {
        int errorMessage = JOptionPane.showOptionDialog(null,
                "Some files are missing error",
                "Error", JOptionPane.CLOSED_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, null, null);
        exitGame();
    }

    /**
     * Method to resize the icon to user referred size.
     * @param imageIcon image that want to be resized
     * @return image that already resized
     */
    public static ImageIcon resize(ImageIcon imageIcon) {
        Image image = imageIcon.getImage();
        Image resizedImage = image.getScaledInstance(95, 95, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}